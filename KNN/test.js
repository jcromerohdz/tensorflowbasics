require('@tensorflow/tfjs-node');
const tf = require('@tensorflow/tfjs');

// https://stephengrider.github.io/JSPlaygrounds/
//
//
// What's the fastest way to learn ML?
// R. The fastest way to learn Ml is to master fundamental operation
// around working with data
//
// R.Strong knowlage of data handling basics makes applying any algrithms trivial
//
//
// Fundamentals
//
// - Features vs Labels
// - Test vs Training sets of data
// - Features Normalization
// - Common data structures (arrys of arrays)
// - Feature Selection
//
// The Plan
// - Learn some fundamentals around Tesorflow JS
// - Go through a couple exercises with TensorFlow
// - Rebuild KNN algortihm using Tensorflow JS
// - Build other algorithms with Tensorflow JS
//
// Tensorflow JS principle is using for working with numbers in arrays of arrays really easy
//
// - Tensor - [[]] [] [[[]]]  dimension
// - Shape -- length

//Apply a sligthly different KNN algorithm in the browser
//with TensorFlow JS and fake data

// Move KNN algorithm to our code editor with real data and run
// in Node JS enviroment

// Do some optimization

// What is the price of a Hose ? ----> Regression ---> Our Goal

// Steps
// Find distance between features and prediction point
// Sort from lowest point to greatest
// Take the top K records
// Average the label value of those top K records

// Row, Column

const features = tf.tensor([
  [-121, 47],
  [-121.2, 46.5],
  [-122, 46.4],
  [-120.9, 46.7]
]);

const labels = tf.tensor([
  [200],
  [250],
  [215],
  [240]
]);

const predictionPoint = tf.tensor([-121, 4]);
const k = 2;

// Find distance between features and prediction prediction point
// rsqrt (latf - latpp ^ 2 + longf - logpp 2)


const knn = (features, labels,  predictionPoint, k) => {
  return features
    .sub(predictionPoint)
    .pow(2)
    .sum(1)
    .pow(.5)
    .expandDims(1) // Sort from lowest point to greatest
    .concat(labels, 1)
    .unstack() // Transform normal JS array
    .sort((a,b)=> a.arraySync()[0] > b.arraySync()[0] ? 1 : -1)
    .slice(0,k)// Take the top k records
    .reduce((acc,pair)=>acc+pair.arraySync()[1],0)/k; //Average the label value of those top k rocord
}


console.log(knn(features, labels, predictionPoint, k));
