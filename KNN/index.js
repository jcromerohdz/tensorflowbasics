require('@tensorflow/tfjs-node');
const tf = require('@tensorflow/tfjs');

const loadCSV = require('./load-csv');

const knn = (features, labels, predictionPoint, k) =>{
  const { mean, variance } = tf.moments(features, 0);

  const scaledPrediction = predictionPoint.sub(mean).div(variance.pow(0.5))



  // Find distance between features and prediction prediction point
  return features
    .sub(mean)
    .div(variance.pow(0.5))
    .sub(scaledPrediction)
    .pow(2)
    .sum(1)
    .pow(.5)
    .expandDims(1) // Sort from lowest point to greatest
    .concat(labels, 1)
    .unstack() // Transform normal JS array
    .sort((a,b)=> a.arraySync()[0]> b.arraySync()[0]? 1:-1)
    .slice(0,k)// Take the top k records
    .reduce((acc,pair)=>acc+pair.arraySync()[1],0)/k; //Average the label value of those top k rocord
};

let { features, labels, testFeatures, testLabels } = loadCSV('kc_house_data.csv', {
  suffle: true,
  splitTest: 10,
  dataColumns: ['lat', 'long', 'sqft_lot'],
  labelColumns: ['price'],
});

features = tf.tensor(features);
labels = tf.tensor(labels);
// testFeatures = tf.tensor(testFeatures);
// testLabels = tf.tensor(testLabels);
const k = 10;

// const result = knn(features, labels, tf.tensor(testFeatures[0]), k);
// console.log(`Predict value: ${result}, Real value: ${testLabels[0][0]}`);
// const err = (testLabels[0][0] - result) / testLabels[0][0];
// console.log(`Error:Thanks for endorsing me for HTML5! ${err * 100} %`)

testFeatures.forEach((testPoint, i) => {
  const result = knn(features, labels, tf.tensor(testPoint), k);
  const err = (testLabels[i][0] - result) / testLabels[i][0];
  console.log(`Error: ${err * 100} %`)
});

// console.log(testFeatures);
// console.log(testLabels);
